# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

import os
import sys

module_path = os.path.abspath(os.path.join('MelGANmusic'))
if module_path not in sys.path:
    sys.path.append(module_path)
from mel2wav import MelVocoder
from mel2wav.utils import save_sample


class Spec2Wav():
    def __init__(root):
        path = os.path.abspath(os.path.join('MelGANMusic/models/melsong'))
        self.vocoder = MelVocoder(path)
        self.root = os.path.abspath(os.path.join(root))
    
    def invert(mel, name):
        wav = self.vocoder.inverse(mel).squeeze().cpu().numpy()
        save_sample(self.root / ("%s.wav" % name), 22050,wav)
