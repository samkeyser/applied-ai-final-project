# applied-ai-final-project
Do we want a licence? I just copied the MIT license from the other project, which allows free usage and distribution, which I think is realistically okay.

`git clone https://gitlab.com/samkeyser/applied-ai-final-project.git`

Supplemental papers:
MelGAN-VC: https://arxiv.org/abs/1910.03713  
MelGAN: https://arxiv.org/abs/1910.06711v3
