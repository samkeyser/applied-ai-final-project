%%%%%%%% ICML 2022 EXAMPLE LATEX SUBMISSION FILE %%%%%%%%%%%%%%%%%

\documentclass[nohyperref]{article}

% Recommended, but optional, packages for figures and better typesetting:
\usepackage{microtype}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{booktabs} % for professional tables
\usepackage{adjustbox}

% hyperref makes hyperlinks in the resulting PDF.
% If your build breaks (sometimes temporarily if a hyperlink spans a page)
% please comment out the following usepackage line and replace
% \usepackage{icml2022} with \usepackage[nohyperref]{icml2022} above.
\usepackage{hyperref}


% Attempt to make hyperref and algorithmic work together better:
\newcommand{\theHalgorithm}{\arabic{algorithm}}

% Use the following line for the initial blind version submitted for review:
\usepackage[accepted]{icml2022}

% If accepted, instead use the following line for the camera-ready submission:
% \usepackage[accepted]{icml2022}

% For theorems and such
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{amsthm}

% if you use cleveref..
\usepackage[capitalize,noabbrev]{cleveref}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% THEOREMS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{assumption}[theorem]{Assumption}
\theoremstyle{remark}
\newtheorem{remark}[theorem]{Remark}

% Todonotes is useful during development; simply uncomment the next line
%    and comment out the line below the next line to turn off comments
%\usepackage[disable,textsize=tiny]{todonotes}
\usepackage[textsize=tiny]{todonotes}


% The \icmltitle you define below is probably too long as a header.
% Therefore, a short form for the running title is supplied here:
\icmltitlerunning{Submission and Formatting Instructions for ICML 2022}

\begin{document}

\twocolumn[
\icmltitle{Musical Style Transfer with MelGAN-VC}

% It is OKAY to include author information, even for blind
% submissions: the style file will automatically remove it for you
% unless you've provided the [accepted] option to the icml2022
% package.

% List of affiliations: The first argument should be a (short)
% identifier you will use later to specify author affiliations
% Academic affiliations should list Department, University, City, Region, Country
% Industry affiliations should list Company, City, Region, Country

% You can specify symbols, otherwise they are numbered in order.
% Ideally, you should not use this facility. Affiliations will be numbered
% in order of appearance and this is the preferred way.
\icmlsetsymbol{equal}{*}

\begin{icmlauthorlist}
\icmlauthor{Sam Keyser}{MSOE}
\icmlauthor{Xavier Robbins}{MSOE}
\icmlauthor{Josh Grant}{MSOE}
%\icmlauthor{Jonny Keane}{MSOE}
%\icmlauthor{Salvin Chowdhury}{MSOE}
%\icmlauthor{Jeremy Kedziora}{MSOE}
\end{icmlauthorlist}

\icmlaffiliation{MSOE}{Electrical Engineering \& Computer Science, Milwaukee School of Engineering, Milwaukee, WI, United States}

\icmlcorrespondingauthor{Sam Keyser}{keysers@msoe.edu}
\icmlcorrespondingauthor{Xavier Robbins}{robbinsx@msoe.edu}
\icmlcorrespondingauthor{Josh Grant}{grantj@msoe.edu}

% You may provide any keywords that you
% find helpful for describing your paper; these are used to populate
% the "keywords" metadata in the PDF but will not be shown in the document
\icmlkeywords{Machine Learning}

\vskip 0.3in
]

% this must go after the closing bracket ] following \twocolumn[ ...

% This command actually creates the footnote in the first column
% listing the affiliations and the copyright notice.
% The command takes one argument, which is text to display at the start of the footnote.
% The \icmlEqualContribution command is standard text for equal contribution.
% Remove it (just {}) if you do not need this facility.

\printAffiliationsAndNotice{}  % leave blank if no need to mention equal contribution
%\printAffiliationsAndNotice{\icmlEqualContribution} % otherwise use the standard text.

\begin{abstract}
    The MelGAN-VC architecture is used to train an audio style transfer model on the GTZAN musical genre dataset. We train a model to convert from metal music to classical music and perform experiments using T-SNE and a variety of clutering techniques to evaluate the quality of the style transfer without subjective listener tests. We validate the generalization of our model by performing rock to classical music style transfer \emph{without} training the model on any rock samples.

\end{abstract}

\section{Introduction}
\label{introduction}
In this project we aimed to utilize a GAN architecture previously used for voice conversion and audio style transfer known as MelGAN-VC, to style transfer music from one genre to another \cite{melgan}.

Style transfer is a machine learning task often associated with GAN architectures and can be applied to several domains. In the case of the MelGAN architecture used in this project, the model was mainly applied to the task of voice conversion, which attempts to make a sound clip from one speaker sound as if it was spoken by a different voice. Style transfer between musical genres however, is a relatively new domain in the task of style transfer.

The goal of this project is to see if an existing audio style transfer architecture for voice conversion is viable in the domain of musical genre style transfer. Additionally, this project also aims to explore an objective measure of style transfer quality by clustering style transferred outputs with the original training data. This is inspired by the lack of industry standard objective measures of style transferred audio. Where most often, model losses are used as an indication of the quality of the style transferred samples which are then typically evaluated through subjective listener tests.

\section{Dataset Description}
The dataset used for this project is the GTZAN dataset, commonly regarded as the MNIST for audio. This dataset was originally introduced in the 2002 paper “Musical Genre Classification of Audio Signals” by George Tzanetakis and Perry Cook \cite{gtzan}. The GTZAN dataset contains 1000 samples of music from 10 common genres of music, from the likes of rock and metal to blues and country. Each genre in the data is represented by 100 samples, each 30 seconds in length.

In the following experiments, the metal and classical genres from the data are used to train the model to transfer metal samples in the style of the classical samples from the data. To explore how the model generalizes with another genre, we test the model by transferring the samples in the rock genre to classical, as rock and metal music often include the same instruments and can have relatively similar sounds.

\section{Model Architecture}
MelGAN-VC is trained over the mel-spectrogram representation of music samples from the GTZAN dataset. Most signals are represented as a change in amplitude over time, but a spectrogram instead represents the strength of each frequency present in the signal over time. Spectrograms are very commonly used in audio applications.

\begin{figure}[h!]
    \centerline{\includegraphics[width=\columnwidth]{spectrograms}}
    \caption{A signal and its corresponding spectrogram (\href{https://www.researchgate.net/figure/Spectrograms-and-Oscillograms-This-is-an-oscillogram-and-spectrogram-of-the-boatwhistle_fig2_267827408}{source})}
    \label{fig:spectrogram}
\end{figure}

The mel spectrogram is a variant of the spectrogram which uses the mel scale. The way human ears perceive pitch doesn’t scale linearly with an increase in frequency, so the mel scale tries to capture this relationship by adjusting the frequency axis to be linear in pitch.

These spectrograms can be thought of as images, which motivated the use of convolutional layers throughout MelGAN. The generator uses a series of 2D convolutional layers with intervening batch normalization and leaky ReLU activation functions, along with an upsampling and transpose convolutional layer. The discriminator is a much shorter network with 3, 2D convolutional layers with intervening leaky ReLU layers, ending in a final dense layer.

\begin{figure}[h!]
    \begin{subfigure}[Generator Network]{\textwidth}
  \centering
  \includegraphics[width=.4\linewidth]{gen}
  \label{fig:generator}
\end{subfigure}%
\begin{subfigure}[Discriminator Network]{\textwidth}
  \centering
  \includegraphics[width=.4\linewidth]{discriminator}
  \label{fig:discrimnator}
\end{subfigure}%
\begin{subfigure}[Siamese Network]{\textwidth}
  \centering
  \includegraphics[width=.4\linewidth]{siam}
  \label{fig:siamese}
\end{subfigure}%
\caption{Model architectures}
\end{figure}

Each convolutional layer also includes spectral normalization, which is a weight normalization technique that has been shown to have beneficial effects for training discriminator networks in GAN architectures \cite{specnorm}.

A third network, the siamese network which we will describe later, also uses a set of 2D convolutional layers, batch normalization, leaky ReLU activations, and a final dense layer. It does not use spectral normalization.

MelGAN-VC is capable of handling inputs of arbitrary length. It does this by chunking input signals along the time axis and processing each individually. A full length output can be reconstructed by concatenating each output chunk back into a full length sample.

In addition to the traditional GAN architecture the authors introduce two additional loss terms: Identity Loss and TraVeL Loss. The identity loss penalizes the network in proportion to the expected value of the squared difference of original chunks aji, and their mapped representations G(aji) for a preset sample size. This aids the network in learning the “least effort” mapping between domains, as the expected value of the squared difference will rise the more changes the generator makes.

The TraVel loss \cite{travelgan} is more involved and for the sake of brevity we touch on it only briefly. Before feeding the input mel spectrogram into the model, the spectrogram is again split in half along the time axis forming a chunklet. Each chunklet is fed into both the generator network and a cooperative siamese network S individually. The siamese network encodes each chunklet into a latent space representation and the translation between the encoded chunklets in the same domain is computed. The TraVel loss penalizes the network whenever the translation between the encoded chunklets differs in the pre- and post- generator samples. This helps the generator preserve content without relying on pixel-wise losses.

\begin{figure}[h!]
    \centerline{\includegraphics[width=\columnwidth]{high-level-arch}}
    \caption{Full MelGAN-VC architecture}
    \label{fig:high-level-arch}
\end{figure}

The high level architecture can be seen in figure \ref{fig:high-level-arch}.

\section{Optimization}
For optimization, we used the RMSProp optimizer technique as implemented by Keras\footnote{\href{https://keras.io/api/optimizers/rmsprop/}{https://keras.io/api/optimizers/rmsprop/}}. RMSProp is an adaptation of the RProp full batch optimization algorithm for mini-batch optimization. RProp addresses the issue of trying to find a global learning rate by using the sign of the gradient. If a weight is consistently moving in the same direction (as measured by the sign of its last couple gradients) then its learning rate should be accelerated, if it has to back track (differing signs) then the “brakes” should be pumped and its learning rate decreased. RMSProp uses this same idea of individual weight learning rates, but uses the moving average of the squared gradients, normalized by its square root. RMSProp enables fast movement through parameter space.

MelGAN-VC uses the Adam optimizer \footnote{\href{https://keras.io/api/optimizers/adam/}{https://keras.io/api/optimizers/adam/}} by default, but we found that using RMSProp led to the generator loss decreasing much more rapidly than the Adam optimizer. We had observed in previous work for this course that the RMSProp algorithm tends to induce good performance on the training set very quickly, but often doesn’t converge on the testing set within the same timeframe. To address this concern we ran a set of validation experiments described in the following section.

\section{Results}
In this project two versions of the MelGAN-VC architecture were tested and compared. The first model, which acted as our baseline, was trained over 500 epochs with the Adam optimizer with a learning rate 0.0002 and batch size of 16. The second, optimized model was trained with the RMSProp optimizer as described above. Similar to the baseline, this model was trained with a learning rate of 0.0002 with a batch size of 16 for 500 epochs. Each model was trained to style transfer metal music to classical music. To further evaluate how our model generalizes after training, we also style transfer the rock samples from the dataset as it can have similar instruments and musical elements to metal music and we would expect the models to perform similarly when style transferring these samples.

To examine model performance, we examined the T-SNE representation of the metal + classical samples within our dataset, as well as samples which had been style transferred from metal to classical. We performed clustering over the representation to see if genres clustered together. There is currently no state of the art method for determining the quality of audio style transfer objectively. Often quality of style transferred audio is examined subjectively through listening tests or through the model loss, which does not provide a full view of the quality of the models output.  Finally, we repeated our experiments with rock + classical, using rock as a validation set since the model was not trained on rock→classical.

The first set of tests examines how samples are transferred from metal to classical, the combination that the model was trained on. To begin, the model style transfers the metal samples to classical, then a set of the original metal, original classical, and style transferred classical mel spectrograms are created. These spectrograms are then flattened and the features are passed through TSNE and clustered using three different clustering algorithms. The clustering algorithms tested are K Means, Hierarchical with Ward linkage, and Spectral Clustering as implemented in Scikit-Learn. The second test completes the same procedure, except style transferring rock samples to classical with the goal of exploring how well the model generalizes its performance to a new pairing of genres.

\subsection{Baseline Model}

\begin{figure}[h!]
    \centerline{\includegraphics[width=\columnwidth]{tsne-baseline}}
    \caption{TSNE Dimensionality reduction applied to baseline model style transferred classical samples and original metal and classical samples. The red line shows the change in position that a metal song undergoes when its style is transferred to a classical song.}
    \label{fig:tsne-baseline}
\end{figure}

\begin{figure}[h!]
    \centerline{\includegraphics[width=\columnwidth]{tsne-baseline-clustering}}
    \caption{TSNE Dimensionality reduction applied to baseline model style transferred classical samples and original dataset metal and classical samples. The plots above show the results of clustering with KMeans, Ward Hierarchical, and Spectral clustering methods. }
    \label{fig:tsne-baseline-clustering}
\end{figure}


Figure \ref{fig:tsne-baseline} shows the T-SNE dimensionality reduction of the metal and classical samples in green and orange with the style transferred classical samples in blue. The red line indicates how a metal sample moves when style transferred with the baseline model. Here we see that the transferred sample is moved towards a small cluster of dataset classical samples indicating that the model has some ability to transfer a sample to the classical genre.

The results of the three clustering algorithms on this data can be seen in figure \ref{fig:tsne-baseline-clustering}. Two clusters are created with each algorithm with the intention that the style transferred classical and dataset classical samples are clustered together and the dataset metal samples are clustered separately. However, since the genres are not well separated when reduced with TSNE the clustering algorithms struggle to form pure clusters with the metal and combined classical samples.

\begin{figure}[h!]
    \centerline{\includegraphics[width=\columnwidth]{tsne-baseline-validation}}
    \caption{TSNE Dimensionality reduction applied to baseline model style transferred classical samples and the dataset rock and classical samples. The red line shows the change in position that a rock song undergoes when its style is transferred to a classical song.}
    \label{fig:tsne-baseline-validation}
\end{figure}

\begin{figure}[h!]
    \centerline{\includegraphics[width=\columnwidth]{tsne-baseline-validation-clustering}}
    \caption{TSNE Dimensionality reduction applied to baseline model style transferred classical samples and original dataset rock and classical samples. The plots above show the results of clustering with KMeans, Ward Hierarchical, and Spectral clustering methods.}
    \label{fig:tsne-baseline-validation-clustering}
\end{figure}

In figure \ref{fig:tsne-baseline-validation} we observe similar results when exploring how the model style transfers rock music to classical music. Here again we see that the genres are not well separated by the dimensionality reduction method. As a result the clustering algorithms again do not form well defined clusters with the classical and rock samples (see figure \ref{fig:tsne-baseline-validation-clustering}). We again also plot a line between an original rock sample and its style transferred counterpart to examine how the sample moves when transferred by the model.

\subsection{Optimized Model}
\begin{figure}[h!]
    \centerline{\includegraphics[width=\columnwidth]{tsne-optimized}}
    \caption{TSNE Dimensionality reduction applied to the optimized model style transferred classical samples and original metal and classical samples. The red line shows the change in position that a metal song undergoes when its style is transferred to a classical song.}
    \label{fig:tsne-optimized}
\end{figure}

\begin{figure}[h!]
    \centerline{\includegraphics[width=\columnwidth]{tsne-optimized-clustering}}
    \caption{TSNE Dimensionality reduction applied to optimized model style transferred classical samples with original dataset metal and classical samples. The plots above show the results of clustering with KMeans, Ward Hierarchical, and Spectral clustering methods.}
    \label{fig:tsne-optimized-clustering}
\end{figure}

The optimized model using RMSProp was tested with the same procedure as the baseline model. Figures \ref{fig:tsne-optimized}, \ref{fig:tsne-optimized-clustering} show the dimensionality reduced metal and classical dataset samples and the style transferred classical samples, and the clustering results with the three above mentioned algorithms. Here we also see how the optimized model moves a point across the graph. Like the baseline model we see that the style transferred point is moved toward a small cluster of dataset classical points indicating its ability to transfer a metal sample to classical. The clustering results are again poor due to the poor separation of genres are not well separated by dimensionality reduction.

\begin{figure}[h!]
    \centerline{\includegraphics[width=\columnwidth]{tsne-optimized-validation}}
    \caption{TSNE Dimensionality reduction applied to optimized model style transferred classical samples with original metal and classical samples. The red line shows the change in position that a metal song undergoes when its style is transferred to a classical song. }
    \label{fig:tsne-optimized-validation}
\end{figure}

\begin{figure}[h!]
    \centerline{\includegraphics[width=\columnwidth]{tsne-optimized-validation-clustering}}
    \caption{TSNE Dimensionality reduction applied to optimized model style transferred classical samples with original dataset rock and classical samples. The plots above show the results of clustering with KMeans, Ward Hierarchical, and Spectral clustering methods.}
    \label{fig:tsne-optimized-validation-clustering}
\end{figure}

Figures \ref{fig:tsne-optimized-validation}, \ref{fig:tsne-optimized-validation-clustering} contain the results of style transferring rock music to classical with the RMSProp model. We see similar results to the baseline in this case as well, as the clusters produced with clustering are not well defined, and dimensionality reduction does not well separate the genres. However looking at how a point moves as it is style transferred we see that the rock music point does move toward a small cluster of the classical samples in the center of the plot.

\section{Reflection}
This project pulled together a lot of the concepts discussed in class from convolutional neural networks, to dimensionality reduction \& clustering, feature engineering, and the mathematical underpinnings of deep learning. While the use of convolutional neural networks for processing the “images” (spectrograms) we had was obvious, a great deal of work was done in engineering the loss function to guide the learning of the network which we thought was very interesting. 

The way that GAN set up used the discriminator to flow gradients back to the generator showed how networks can be combined and how they can share information. Additionally, the way that TraVeL loss and identity loss were used to minimize content drift between the sample and target domains was very interesting. It really felt like the author was doing “loss engineering” to guide the learning of his model, which based on the math we went over this semester makes sense why that would work. We had not considered the loss as a major vector for influencing our deep learning models, beyond using one of several canned loss functions, but this work really showcased how powerful the loss can be especially when there is not an obvious target variable to compare against.

Style transfer networks, especially for music, have applications outside of the classroom in the entertainment industry. Style transfer may allow artists to easily experiment with different styles before committing to mastering a particular style, or to more easily remaster old tracks for new applications. It also offers possibilities for modernizing older works by no-longer-present artists. Finally, style transfer networks have some application as a data augmentation technique \cite{style-transfer-app}, which with music style transfer may assist in training networks to recover or repair old / damaged  audio samples.

% In the unusual situation where you want a paper to appear in the
% references without citing it in the main text, use \nocite
\nocite{langley00}

\bibliography{final-report}
\bibliographystyle{icml2022}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% APPENDIX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\newpage
%\appendix
%\onecolumn
%\section{Policy Results on Training / Testing Corpii}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\end{document}


% This document was modified from the file originally made available by
% Pat Langley and Andrea Danyluk for ICML-2K. This version was created
% by Iain Murray in 2018, and modified by Alexandre Bouchard in
% 2019 and 2021 and by Csaba Szepesvari, Gang Niu and Sivan Sabato in 2022. 
% Previous contributors include Dan Roy, Lise Getoor and Tobias
% Scheffer, which was slightly modified from the 2010 version by
% Thorsten Joachims & Johannes Fuernkranz, slightly modified from the
% 2009 version by Kiri Wagstaff and Sam Roweis's 2008 version, which is
% slightly modified from Prasad Tadepalli's 2007 version which is a
% lightly changed version of the previous year's version by Andrew
% Moore, which was in turn edited from those of Kristian Kersting and
% Codrina Lauth. Alex Smola contributed to the algorithmic style files.
